package br.com.caelum.leilao.servico;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.mockito.InOrder;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.infra.dao.RepositorioDeLeiloes;
import br.com.caelum.leilao.infra.email.EnviadorDeEmail;

public class EncerradorDeLeilaoTest {

	@Test
	public void deveEncerrarLeiloesQueComecaramUmaSemanaAntes() {
		Calendar antiga = Calendar.getInstance();
		antiga.set(1999, 1, 20);
		
		Leilao leilao1 = new CriadorDeLeilao().para("TV de plasma").naData(antiga).constroi();
		Leilao leilao2 = new CriadorDeLeilao().para("Geladeira").naData(antiga).constroi();
		List<Leilao> leiloesAntigos = Arrays.asList(leilao1, leilao2);
		
		RepositorioDeLeiloes daoFalso = mock(RepositorioDeLeiloes.class);
		EnviadorDeEmail cateiroFalso = mock(EnviadorDeEmail.class);

		when(daoFalso.correntes()).thenReturn(leiloesAntigos);

		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(daoFalso, cateiroFalso);
		encerrador.encerra();

		assertEquals(2, encerrador.getTotalEncerrados());
		assertTrue(leilao1.isEncerrado());
		assertTrue(leilao2.isEncerrado());
	}
	
	@Test
	public void naoDeveEncerrarLeiloesQueComecaramOntem() {
		Calendar ontem = Calendar.getInstance();
		ontem.add(Calendar.DAY_OF_MONTH, -1);;
		
		Leilao leilao1 = new CriadorDeLeilao().para("TV de plasma").naData(ontem).constroi();
		Leilao leilao2 = new CriadorDeLeilao().para("Geladeira").naData(ontem).constroi();
		List<Leilao> leiloesAntigos = Arrays.asList(leilao1, leilao2);
		
		RepositorioDeLeiloes daoFalso = mock(RepositorioDeLeiloes.class);
		EnviadorDeEmail cateiroFalso = mock(EnviadorDeEmail.class);

		when(daoFalso.correntes()).thenReturn(leiloesAntigos);
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(daoFalso, cateiroFalso);
		encerrador.encerra();
		
		assertEquals(0, encerrador.getTotalEncerrados());
		assertFalse(leilao1.isEncerrado());
		assertFalse(leilao2.isEncerrado());
	}
	
	@Test
	public void deveFazerNadaQuandoNaoHouverLeilao() {
		RepositorioDeLeiloes daoFalso = mock(RepositorioDeLeiloes.class);
		EnviadorDeEmail cateiroFalso = mock(EnviadorDeEmail.class);

		when(daoFalso.correntes()).thenReturn(new ArrayList<Leilao>());
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(daoFalso, cateiroFalso);
		encerrador.encerra();
		
		List<Leilao> correntes = daoFalso.correntes();
		
		assertEquals(0, correntes.size());
		assertEquals(0, encerrador.getTotalEncerrados());
	}
	
	@Test
	public void deveAtualizarEEnviarEmailDosLeiloesEncerrados() {
		RepositorioDeLeiloes daoFalso = mock(RepositorioDeLeiloes.class);
		EnviadorDeEmail carteiroFalso = mock(EnviadorDeEmail.class);

		Calendar dataAntiga = Calendar.getInstance();
		dataAntiga.set(2000, 1, 1);
		
		Leilao leilao = new CriadorDeLeilao().para("IPhone X").naData(dataAntiga).constroi();
		
		when(daoFalso.correntes()).thenReturn(Arrays.asList(leilao));
		
		EncerradorDeLeilao encerradorDeLeilao = new EncerradorDeLeilao(daoFalso, carteiroFalso);
		encerradorDeLeilao.encerra();
		InOrder order = inOrder(daoFalso, carteiroFalso);
		order.verify(daoFalso, times(1)).atualiza(leilao);
		order.verify(carteiroFalso, times(1)).envia(leilao);
	}
	
    @Test
    public void naoDeveEncerrarLeiloesQueComecaramMenosDeUmaSemanaAtras() {

        Calendar ontem = Calendar.getInstance();
        ontem.add(Calendar.DAY_OF_MONTH, -1);

        Leilao leilao1 = new CriadorDeLeilao().para("TV de plasma")
            .naData(ontem).constroi();
        Leilao leilao2 = new CriadorDeLeilao().para("Geladeira")
            .naData(ontem).constroi();

        RepositorioDeLeiloes daoFalso = mock(RepositorioDeLeiloes.class);
		EnviadorDeEmail cateiroFalso = mock(EnviadorDeEmail.class);
		when(daoFalso.correntes()).thenReturn(Arrays.asList(leilao1, leilao2));

        EncerradorDeLeilao encerrador = new EncerradorDeLeilao(daoFalso, cateiroFalso);
        encerrador.encerra();

        assertEquals(0, encerrador.getTotalEncerrados());
        assertFalse(leilao1.isEncerrado());
        assertFalse(leilao2.isEncerrado());

        verify(daoFalso, never()).atualiza(leilao1);
        verify(daoFalso, never()).atualiza(leilao2);
    }
    
    @Test
    public void deveContinuarAExecucaoMesmoQuandoDaoFalha() {
		Calendar dataAntiga = Calendar.getInstance();
		dataAntiga.set(2000, 1, 1);
		
		Leilao leilao1 = new CriadorDeLeilao().para("IPhone X").naData(dataAntiga).constroi();
		Leilao leilao2 = new CriadorDeLeilao().para("Carreta furac�o").naData(dataAntiga).constroi();
		
		RepositorioDeLeiloes daoFalso = mock(RepositorioDeLeiloes.class);
		EnviadorDeEmail carteiroFalso = mock(EnviadorDeEmail.class);
		
		when(daoFalso.correntes()).thenReturn(Arrays.asList(leilao1, leilao2));
		doThrow(new RuntimeException()).when(daoFalso).atualiza(leilao1);
		
		EncerradorDeLeilao encerradorDeLeilao = new EncerradorDeLeilao(daoFalso, carteiroFalso);
		encerradorDeLeilao.encerra();
		
		verify(daoFalso).atualiza(leilao2);
		verify(carteiroFalso).envia(leilao2);
		
		verify(carteiroFalso, never()).envia(leilao1);
    }
    
    @Test
    public void naoDeveEnviarEmailQuandoTodosFalham() {
    	Calendar dataAntiga = Calendar.getInstance();
    	dataAntiga.set(2000, 1, 1);
    	
    	Leilao leilao1 = new CriadorDeLeilao().para("IPhone X").naData(dataAntiga).constroi();
    	Leilao leilao2 = new CriadorDeLeilao().para("Carreta furac�o").naData(dataAntiga).constroi();
    	
    	RepositorioDeLeiloes daoFalso = mock(RepositorioDeLeiloes.class);
    	EnviadorDeEmail carteiroFalso = mock(EnviadorDeEmail.class);
    	
    	when(daoFalso.correntes()).thenReturn(Arrays.asList(leilao1, leilao2));
    	doThrow(new RuntimeException()).when(daoFalso).atualiza(any(Leilao.class));
    	
    	EncerradorDeLeilao encerradorDeLeilao = new EncerradorDeLeilao(daoFalso, carteiroFalso);
    	encerradorDeLeilao.encerra();
    	
    	verify(carteiroFalso, never()).envia(leilao1);
    	verify(carteiroFalso, never()).envia(leilao2);
    }
    
//	@Test
//	public void testeMock() {
//		LeilaoDao daoFalso = mock(LeilaoDao.class);
//		when(daoFalso.teste()).thenReturn("aaa");
//		assertEquals("aaa", daoFalso.teste());
//	}
}
