package br.com.caelum.leilao.servico;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Pagamento;
import br.com.caelum.leilao.dominio.Usuario;
import br.com.caelum.leilao.infra.Relogio;
import br.com.caelum.leilao.infra.dao.RepositorioDeLeiloes;
import br.com.caelum.leilao.infra.dao.RepositorioDePagamentos;

public class GeradorDePagamentoTest {

	@Test
	public void deveGerarPagamentoParaUmLeilaoEncerrado() {
		RepositorioDeLeiloes leiloes = mock(RepositorioDeLeiloes.class);
		RepositorioDePagamentos pagamentos = mock(RepositorioDePagamentos.class);
		Avaliador avaliador = new Avaliador();
		
		Leilao leilao = new CriadorDeLeilao().para("Monalisa")
				.lance(new Usuario("Jo�ozinho"), 2000.0)
				.lance(new Usuario("maria da silva"), 4550.0)
				.constroi();
		
		when(leiloes.encerrados()).thenReturn(Arrays.asList(leilao));
		
		GeradorDePagamento geradorDePagamento = new GeradorDePagamento(leiloes, pagamentos, avaliador);
		geradorDePagamento.gera();
		
		ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
		verify(pagamentos).salva(argumento.capture());
		
		Pagamento pagamento = argumento.getValue();
		assertEquals(4550.0, pagamento.getValor(), 0.0001);
	}
	
	@Test
	public void deveGerarPagamentoParaSegundaQuandoForSabado() {
		RepositorioDeLeiloes leiloes = mock(RepositorioDeLeiloes.class);
		RepositorioDePagamentos pagamentos = mock(RepositorioDePagamentos.class);
		Relogio relogio = mock(Relogio.class);
		
		Calendar sabado = Calendar.getInstance();
		sabado.set(2020, Calendar.JANUARY, 25);
		
		when(relogio.agora()).thenReturn(sabado);
		
		when(leiloes.encerrados()).thenReturn(Arrays.asList(new CriadorDeLeilao().para("Monalisa")
				.lance(new Usuario("Jo�ozinho"), 2000.0)
				.lance(new Usuario("maria da silva"), 4550.0)
				.constroi()));
		
		GeradorDePagamento geradorDePagamento = new GeradorDePagamento(leiloes, pagamentos, new Avaliador(), relogio);
		geradorDePagamento.gera();
		
		ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
		verify(pagamentos).salva(argumento.capture());
		
		Pagamento pagamentoGerado = argumento.getValue();
		assertEquals(27, pagamentoGerado.getData().get(Calendar.DAY_OF_MONTH));
	}
	
	@Test
	public void deveGerarPagamentoParaSegundaQuandoForDomingo() {
		RepositorioDeLeiloes leiloes = mock(RepositorioDeLeiloes.class);
		RepositorioDePagamentos pagamentos = mock(RepositorioDePagamentos.class);
		Relogio relogio = mock(Relogio.class);
		
		Calendar sabado = Calendar.getInstance();
		sabado.set(2020, Calendar.JANUARY, 26);
		
		when(relogio.agora()).thenReturn(sabado);
		
		when(leiloes.encerrados()).thenReturn(Arrays.asList(new CriadorDeLeilao().para("Monalisa")
				.lance(new Usuario("Jo�ozinho"), 2000.0)
				.lance(new Usuario("maria da silva"), 4550.0)
				.constroi()));
		
		GeradorDePagamento geradorDePagamento = new GeradorDePagamento(leiloes, pagamentos, new Avaliador(), relogio);
		geradorDePagamento.gera();
		
		ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
		verify(pagamentos).salva(argumento.capture());
		
		Pagamento pagamentoGerado = argumento.getValue();
		assertEquals(27, pagamentoGerado.getData().get(Calendar.DAY_OF_MONTH));
	}
}
